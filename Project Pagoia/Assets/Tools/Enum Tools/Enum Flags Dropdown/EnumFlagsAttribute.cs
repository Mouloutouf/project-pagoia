using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumFlagsAttribute : PropertyAttribute
{
    public Type type;
    public EnumFlagsAttribute(Type _type)
    {
        this.type = _type;
    }
}

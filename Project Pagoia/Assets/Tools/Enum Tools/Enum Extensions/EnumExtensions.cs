using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnumExtensions
{
    public static bool Has<T>(this Enum type, T value) where T : Enum
    {
        try {
            return (((int)(object)type & (int)(object)value) == (int)(object)value);
        }
        catch {
            return false;
        }
    }
    public static T Add<T>(this Enum type, T value) where T : Enum
    {
        // WHAT THIS CODE REALLY DOES :
        // return type | value;

        // The double casts (int)(object) are required because for some reason, C# is unable to cast an enum directly into an int.
        // So you need that extra (object) cast. What do you know !
        // Same for the (T)(object) double cast, C# is unable to cast directly from int to T (which here is Enum)

        // And you can't do : return type | value; either
        // Because you can't do Enum | T, even though T is Enum.
        // Which is why you need to first cast both parameters to int, then cast it back to T (Enum)
        return (T)(object)(((int)(object)type | (int)(object)value));
    }

    public static bool HasAll<T>(this Enum type, params T[] flags) where T : Enum
    {
        foreach (var f in flags)
        {
            if (((int)(object)type & (int)(object)f) != (int)(object)f)
                return false;
        }
        return true;
    }
}

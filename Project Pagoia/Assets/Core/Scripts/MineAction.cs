﻿namespace Core.Scripts
{
    public class MineAction : ActionBehavior
    {
        private Features.Scripts.OreBlock targetBlock;

        protected override bool Check()
        {
            return targetBlock.Mined;
        }

        public override void StartAction()
        {
            targetBlock = Target.GetComponent<Features.Scripts.OreBlock>();
            targetBlock.StartMining();
        }
        public override void StopAction() { }
    }
}

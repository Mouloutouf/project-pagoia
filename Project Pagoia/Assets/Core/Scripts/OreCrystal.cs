﻿using UnityEngine;

namespace Core.Scripts
{
    public class OreType : ScriptableObject
    {
        public GameObject orePrefab;
    }
    
    public class OreCrystal : ItemEntity
    {
        public OreType oreType;
    }
}
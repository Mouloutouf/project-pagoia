﻿using UnityEngine;

namespace Core.Scripts
{
    [CreateAssetMenu(fileName = "New Action", menuName = "Scriptable Object/Action")]
    public class ActionScriptable : ScriptableObject
    {
        public EntityProperty entityTarget;

        public int baseCost;

        public StateTemplate[] requiredStates;
        public StateTemplate[] satisfiedStates;
    }
}

﻿using Features.Scripts;
using UnityEngine;

namespace Core.Scripts
{
    public class CraftItemBehaviour : MonoBehaviour
    {
        public CraftingTable craftingTable;

        public Entity ItemEntity { get; set; }

        public LoadingBar loadingBar;

        public CraftRecipe Recipe { get; set; }

        public void StartCrafting()
        {
            loadingBar.slider.gameObject.SetActive(true);
            loadingBar._afterLoadAction_ = Craft;
            StartCoroutine(loadingBar.Load());
        }

        public void Craft()
        {
            ItemEntity = Instantiate(Recipe.itemPrefab, craftingTable.craftSpawn).GetComponent<Entity>();
        }
    }
}

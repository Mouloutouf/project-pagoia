﻿using UnityEngine;

namespace Core.Scripts
{
    public class OreBlockType : ScriptableObject
    {
        public OreType oreType;

        public GameObject oreBlockPrefab;
    }
    
    public class OreBlock : MineBlock
    {
        public int oreCount;
    }
}
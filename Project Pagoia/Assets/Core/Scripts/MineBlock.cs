﻿namespace Core.Scripts
{
    public interface IMinable
    {
        public void Mine();
    }
    
    public abstract class MineBlock : VisualEntity, IMinable
    {
        public void Mine()
        {
            throw new System.NotImplementedException();
        }
    }
}
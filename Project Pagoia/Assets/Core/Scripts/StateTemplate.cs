﻿using System;

namespace Core.Scripts
{
    public enum TargetType { SameAsTarget = 0, NewEntity = 1 }

    [Serializable]
    public class StateTemplate
    {
        public StateType stateType;
    
        public bool anyEntity;

        public TargetType targetedEntity;
    }
}
﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Core.Scripts
{
    public enum ResourceType { Iron, Diamond, Wood }

    public enum ItemType { Pickaxe, Sword, Bow, Arrows, Shield }

    public class CraftRecipe : SerializedScriptableObject
    {
        public ItemType itemToCraft;
        public int outputNumber;

        public Dictionary<ResourceType, int> resourcesRequired;

        public GameObject itemPrefab;
    }
}
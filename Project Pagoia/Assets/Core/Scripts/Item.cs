﻿namespace Core.Scripts
{
    public interface ICraftable
    {
        // public Recipe
        public void Craft(int _quantity = 1);
    }

    public interface ISpawnable
    {
        public void Spawn();
    }

    public interface IPickable
    {
        public void Pickup();
        public void Drop();
    }
    
    public interface IUsable
    {
        public void Use();
    }
    
    public abstract class ItemEntity : VisualEntity, IPickable, ISpawnable
    {
        public void Pickup()
        {
            throw new System.NotImplementedException();
        }

        public void Drop()
        {
            throw new System.NotImplementedException();
        }

        public void Spawn()
        {
            throw new System.NotImplementedException();
        }
    }

    public abstract class ToolEntity : ItemEntity, ICraftable, IUsable
    {
        public void Craft(int _quantity = 1)
        {
            throw new System.NotImplementedException();
        }
        
        public void Use()
        {
            throw new System.NotImplementedException();
        }
    }
}
using UnityEngine;

namespace Core.Scripts
{
    public abstract class ActionBehavior : MonoBehaviour
    {
        public Agent agent;

        public ActionScriptable actionData;
        public Action action { get; set; }

        public Entity Target => action.target;

        public bool active { get; set; }

        public abstract void StartAction();
        public abstract void StopAction();
        protected abstract bool Check();

        private void Update()
        {
            if (!active)
                return;

            if (!Check())
                return;
            
            StopAction();

            foreach (var st in action.satisfiedStates)
            {
                if (st.TargetProperties.HasFlag(Target.EntityProperties))
                {
                    World.instance.AddState(st.stateType, Target, agent.entity);
                }
            }

            active = false;
            agent.NextAction();
        }
    }
}

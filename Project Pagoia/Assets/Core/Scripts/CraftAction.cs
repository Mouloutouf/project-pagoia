namespace Core.Scripts
{
    public class CraftAction : ActionBehavior
    {
        private CraftItemBehaviour craftBehavior;
        public CraftRecipe CurrentRecipe { get; set; }

        protected override bool Check() { return false; }

        public override void StartAction()
        {
            craftBehavior = Target.GetComponent<CraftItemBehaviour>();
            craftBehavior.Recipe = CurrentRecipe;
            craftBehavior.StartCrafting();
        }

        public override void StopAction() { }
    }
}

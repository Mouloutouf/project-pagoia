using Sirenix.OdinInspector;
using UnityEngine;

namespace Core.Scripts
{
    public class Team : MonoBehaviour
    {
        [SerializeField, ReadOnly]
        private TeamScriptable teamData;
        [SerializeField]
        private MeshRenderer meshRenderer;

        public void SetTeam(TeamScriptable _teamData)
        {
            teamData = _teamData;
            meshRenderer.material = _teamData.colorMat;
        }
    }
}

using UnityEngine;

namespace Core.Scripts
{
    public class Entity : MonoBehaviour, IEntity
    {
        public string Name { get; }
        public Transform Transform => transform;
    
        public void AddEntityAsTarget(StateType _stateType)
        {
            World.instance.AddStateToEntity(this, _stateType);
        }
        public void RemoveEntityAsTarget(StateType _stateType)
        {
            World.instance.RemoveStateFromEntity(this, _stateType);
        }

        private void Start() => AddEntityAsTarget(StateType.Exists);
        private void OnDisable() => RemoveEntityAsTarget(StateType.Exists);
    }

    public class VisualEntity : Entity, IVisualEntity
    {
        [SerializeField] private GameObject model;
        public GameObject Model => model;
    }
}
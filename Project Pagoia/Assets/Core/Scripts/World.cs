using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public class World : MonoBehaviour
    {
        public static World instance;
        
        private void Awake()
        {
            instance ??= this;
        }

        private readonly Dictionary<IEntity, HashSet<StateType>> entitiesStates = new Dictionary<IEntity, HashSet<StateType>>();

        public List<IEntity> GetEntities(Type _entityType, StateType[] _statesToInclude, StateType[] _statesToExclude)
        {
            List<IEntity> entities = new List<IEntity>();
            foreach (KeyValuePair<IEntity, HashSet<StateType>> entityStatesPair in entitiesStates)
            {
                if (entityStatesPair.Key.GetType() != _entityType)
                    continue;
                
                bool isValid = true;
                
                if (_statesToInclude != null)
                {
                    foreach (StateType includedStateType in _statesToInclude)
                    {
                        if (entityStatesPair.Value.Contains(includedStateType) == false)
                            isValid = false;
                    }
                }
                if (_statesToExclude != null)
                {
                    foreach (StateType excludedStateType in _statesToExclude)
                    {
                        if (entityStatesPair.Value.Contains(excludedStateType))
                            isValid = false;
                    }
                }
                
                if (isValid)
                    entities.Add(entityStatesPair.Key);
            }
            return entities;
        }

        public bool EntityOfTypeHasState(Type _entityType, StateType _stateType)
        {
            foreach (IEntity entity in entitiesStates.Keys)
            {
                if (entity.GetType() != _entityType)
                    continue;

                return EntityHasState(entity, _stateType);
            }
            return false;
        }
        
        public bool EntityHasState(IEntity _entity, StateType _stateType)
        {
            return entitiesStates.ContainsKey(_entity) && entitiesStates[_entity].Contains(_stateType);
        }
        
        public HashSet<StateType> TryGetEntityStates(IEntity _entity)
        {
            if (entitiesStates.ContainsKey(_entity))
                return entitiesStates[_entity];

            return null;
        }
        
        public void AddStateToEntity(IEntity _entity, StateType _stateType)
        {
            if (EntityHasState(_entity, _stateType))
                return;

            if (entitiesStates.ContainsKey(_entity) == false)
                entitiesStates[_entity] = new HashSet<StateType>();

            entitiesStates[_entity].Add(_stateType);
            Debug.Log($"Added State {_stateType} to Entity {_entity}");
        }
        
        public void RemoveStateFromEntity(IEntity _entity, StateType _stateType)
        {
            if (EntityHasState(_entity, _stateType) == false)
                return;
            
            entitiesStates[_entity].Remove(_stateType);
            Debug.Log($"Removed State {_stateType} from Entity {_entity}");
        }
    }
}

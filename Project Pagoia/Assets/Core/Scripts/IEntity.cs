﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public interface IEntity
    {
        public string Name { get; }
        public Transform Transform { get; }
        public Vector3 Position => Transform.position;
    }
    
    public interface IVisualEntity : IEntity
    {
        public GameObject Model { get; }
    }

    public interface IMovingEntity : IEntity
    {
        // public MoveBehaviour
        public void Move(Vector3 _position);
    }

    public interface IPickerEntity : IEntity
    {
        public void Pickup(IPickable _entityToPickup);
        public void Drop(IPickable _entityToDrop);
    }

    public interface ICarrierEntity : IEntity
    {
        public List<ItemEntity> Items { get; }
    }

    public interface IUserEntity : IEntity
    {
        public void Use(IUsable _entityToUse);
    }
}
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Core.Scripts
{
    public class Agent : SerializedMonoBehaviour
    {
        public Entity entity;

        [SerializeField] private Dictionary<GoalType, Goal> goals;
        private readonly List<Goal> currentGoals = new List<Goal>();
    
        private int priority; 
        public int Priority { get => priority; set => priority = Mathf.Clamp(value, 0, currentGoals.Count); }
    
        private Goal current;

        public List<ActionBehavior> availableActionBehaviors = new List<ActionBehavior>();

        private Action[] currentActionPlan;
        private int currentActionIndex;

        public bool IsActive { get; private set; }

        private void Start()
        {
            foreach (var goal in goals.Values)
                currentGoals.Add(goal);
        
            currentGoals.OrderBy(_goal => _goal.priority);

            Priority = 0;
            current = currentGoals[Priority];
        
            StartPlan();
        }

        public void StartPlan()
        {
            currentActionPlan = Planner.CreatePlan(current, this);

            if (Planner.Status == false)
            {
                Debug.LogWarning($"Plan failed with goal {current} and Agent {this}");
            
                Priority++;
                if (Priority >= currentGoals.Count)
                {
                    Debug.LogWarning($"No more goals, Agent {this} dismissed");
                    return;
                }
                current = currentGoals[Priority];
            
                StartPlan();
            }
            else
            {
                IsActive = true;
                foreach (var action in currentActionPlan)
                {
                    Debug.LogWarning($"Action {action.actionData.name} with Target {action.target}");

                    var actionBehaviorQuery =
                        from actionBehavior in availableActionBehaviors where (actionBehavior.actionData == action.actionData) select actionBehavior;
                    action.actionBehavior = actionBehaviorQuery.Single();
                }

                currentActionIndex = 0;
                StartActionBehavior(currentActionIndex);
            }
        }

        public void StartActionBehavior(int _index)
        {
            var actionBehavior = currentActionPlan[_index].actionBehavior;
        
            actionBehavior.action = currentActionPlan[_index];
            actionBehavior.active = true;
            actionBehavior.StartAction();
        }
        public void NextAction()
        {
            currentActionIndex++;
            if (currentActionIndex >= currentActionPlan.Length)
            {
                Debug.Log("Hurray ! Goal is complete !");

                Priority = 0; // Do it again
                current = currentGoals[Priority];

                StartPlan();
            }
            else
                StartActionBehavior(currentActionIndex);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Scripts
{
    public static class Planner
    {
        public static bool Status { get; private set; }
    
        public static Action[] CreatePlan(Goal _goal, Agent _agent)
        {
            Debug.LogWarning($"Starting Plan for Goal {_goal.name} with Agent {_agent}");

            var availableActionsQuery =
                from actionBehavior in _agent.availableActionBehaviors select actionBehavior.actionData;
            var availableActions = availableActionsQuery.ToList();
        
            Action[] plan = FindPlan(availableActions, _goal.requiredState, _agent);

            // If the plan is empty, it means it has failed at some point in the process
            Status = (plan != null);
            return plan;
        }

        private static Action[] FindPlan(List<ActionScriptable> _availableActions, StateTemplate _requiredState, Agent _agent)
        {
            // We need to search for a Valid Target first
            var target = SearchForTarget(_requiredState.TargetProperties, _agent);
            
            if (target == null)
            {
                Debug.LogWarning($"The Goal required cannot be performed, no valid targets were found");
                Status = false;
                
                return null;
            }

            // Start the Search, with the right target
            Debug.Log($"Starting Search for State {_requiredState.stateType} with Target {target}");
            SearchPlan(_availableActions, _requiredState, out var plan, target, _agent);
            
            return plan;
        }
        private static Action[] FindPlan(List<ActionScriptable> _availableActions, StateTemplate _requiredState, IEntity _target, Agent _agent)
        {
            // Start the Search, with the right target
            Debug.Log($"Starting Search for State {_requiredState.stateType} with Target {_target}");
            SearchPlan(_availableActions, _requiredState, out var plan, _target, _agent);
            
            return plan;
        }

        private static IEntity SearchForTarget(Type _entityType, Agent _agent)
        {
            // Find all entities that match the target entity properties
            // In this specific case -> Find all entities with target properties that exist and that are not equipped
            List<IEntity> potentialTargets = World.instance.GetEntities(_entityType, new[] { StateType.Exists }, new[] { StateType.Accessible });
            if (potentialTargets.Count == 0)
            {
                Debug.Log($"Target not found. No entities of type {_entityType} could be found");
                return null;
            }

            // Pick the closest target out of all entities
            float[] distances = new float[potentialTargets.Count];
            for (int i = 0; i < potentialTargets.Count; i++)
                distances[i] = Vector3.Distance(_agent.transform.position, potentialTargets[i].Transform.position);
            
            var closest = Array.IndexOf(distances, distances.Min());
        
            IEntity target = potentialTargets[closest];
            Debug.Log($"Target found ! Target is {target}");
            
            return target;
        }
    
        // Tries to find the best plan possible, with the given actions, based on the required state, and the given target
        private static void SearchPlan(List<ActionScriptable> _availableActions, StateTemplate _requiredState, out Action[] _plan, in IEntity _target, in Agent _agent)
        {
            // Set the status to true. Will become false if the plan fails
            Status = true;

            // Check if the required state is already satisfied, if so then no action plan is needed
            // In case the required state is considered satisfied if any entity satisfies the state
            if (_requiredState.anyEntity)
            {
                if (World.instance.EntityOfTypeHasState(_target.GetType(), _requiredState.stateType))
                {
                    Debug.Log($"State {_requiredState.stateType} with Target Type {_target.GetType()} is already satisfied");
                    _plan = null;
                    
                    return;
                }
            }
            // In case the required state is considered satisfied if the target entity satisfies the state
            else
            {
                if (World.instance.EntityHasState(_target, _requiredState.stateType))
                {
                    Debug.Log($"State {_requiredState.stateType} with Target {_target} is already satisfied");
                    _plan = null;
                    
                    return;
                }
            }

            var potentialPlan = new List<Action>();

            // Search then pick an action
            var potentialActions = SearchForActions(_availableActions, _requiredState, _target);
            if (potentialActions.Length <= 0)
                Debug.Log("PLAN FAILED NO ACTIONS FOUND");
        
            var bestAction = PickBestAction(potentialActions);
        
            if (bestAction == null)
            {
                Debug.Log("PLAN FAILED NO BEST ACTION FOUND");
                Status = false;
                
                _plan = null;
                return;
            }
        
            // Check if the picked action has any required states
            var pickedActionRequiredStates = bestAction.actionData.requiredStates;
            if (pickedActionRequiredStates.Length > 0)
            {
                for (var i = 0; i < pickedActionRequiredStates.Length; i++)
                {
                    Action[] additionalPlan;

                    // Recursion process to find a plan that satisfies the new required state
                    // In case the required state target is different from the current target
                    if (pickedActionRequiredStates[i].targetedEntity == TargetType.NewEntity)
                        additionalPlan = FindPlan(_availableActions, pickedActionRequiredStates[i], _agent);
                    // In case the required state target is the same as the current target
                    else
                        additionalPlan = FindPlan(_availableActions, pickedActionRequiredStates[i], _target, _agent);

                    // Check the additional plan, Add it to the potential plan
                    if (additionalPlan == null)
                    {
                        if (Status == false)
                        {
                            _plan = null;
                            return;
                        }
                        continue;
                    }
                    potentialPlan.AddRange(additionalPlan);
                }
            }

            // Add the picked action to the plan last
            potentialPlan.Add(bestAction);

            // Return the Plan
            _plan = potentialPlan.ToArray();
        }

        private static Action[] SearchForActions(List<ActionScriptable> _actionsToSearch, StateTemplate _requiredState, IEntity _target)
        {
            var actionsFound = new List<Action>();

            foreach (var actionData in _actionsToSearch) // Can grow quite large in size
            {
                foreach (var satisfiedState in actionData.satisfiedStates)
                {
                    if (_requiredState.stateType != satisfiedState.stateType)
                        continue;

                    if (_requiredState.TargetProperties.HasFlag(satisfiedState.TargetProperties))
                    {
                        Debug.Log($"New potential Action {actionData.name} with Target {_target}");
                        var action = new Action { actionData = actionData, target = _target };
                        actionsFound.Add(action);
                    
                        // break from both loops
                    }
                }
            }
            return actionsFound.ToArray();
        }
    
        private static Action PickBestAction(Action[] _actions)
        {
            if (_actions == null || _actions.Length <= 0)
                return null;
        
            // This will calculate the costs of each Action, then order them
            var orderedActions = _actions.OrderBy(_action => _action.Cost);
            var action = orderedActions.First();
        
            Debug.Log($"Picking Action {action.actionData.name} with Target {action.target}");
            
            return action;
        }
    }
}

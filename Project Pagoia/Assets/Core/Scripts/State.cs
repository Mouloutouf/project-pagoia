namespace Core.Scripts
{
    // Make this Scriptable Objects
    public enum StateType { Accessible, Exists, Spawned, Destroyed, Recovered }

    public class State
    {
        public StateType stateType;

        public IEntity target;
    }

    public class ActorState : State
    {
        public IEntity actor;
    }
}
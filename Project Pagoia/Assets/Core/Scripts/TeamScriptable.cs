﻿using UnityEngine;

namespace Core.Scripts
{
    [CreateAssetMenu(fileName = "New Team", menuName = "Scriptable Object/Team")]
    public class TeamScriptable : ScriptableObject
    {
        public Material colorMat;
        public string Name => name;
    }
}

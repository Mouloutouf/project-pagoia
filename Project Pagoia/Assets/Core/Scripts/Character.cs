﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public interface ITeamable
    {
        public int Team { get; } // Make this a Team variable and create Teams as scriptable objects, with colors
    }
    
    public abstract class Character : VisualEntity, IMovingEntity, ICarrierEntity, IPickerEntity, IUserEntity, ITeamable
    {
        [SerializeField] private int team;
        public int Team => team;

        public List<ItemEntity> Items { get; } = new List<ItemEntity>();

        public void Move(Vector3 _position)
        {
            throw new System.NotImplementedException();
        }

        public void Pickup(IPickable _entityToPickup)
        {
            _entityToPickup.Pickup();
        }

        public void Drop(IPickable _entityToDrop)
        {
            _entityToDrop.Drop();
        }
    
        public void Use(IUsable _entityToUse)
        {
            _entityToUse.Use();
        }
    }
}
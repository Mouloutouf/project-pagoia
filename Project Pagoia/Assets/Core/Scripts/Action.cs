﻿namespace Core.Scripts
{
    public class Action
    {
        public ActionScriptable actionData;
        public ActionBehavior actionBehavior;

        public IEntity target;

        public int Cost => CalculateActionCost();

        public StateTemplate[] requiredStates => actionData.requiredStates;
        public StateTemplate[] satisfiedStates => actionData.satisfiedStates;

        private int CalculateActionCost()
        {
            var cost = 0;

            foreach (var st in requiredStates)
            {
                if (st.targetedEntity == TargetType.NewEntity) {
                    if (World.instance.ContainsState(st.stateType, st.TargetProperties) == false) cost++;
                }
                else if (st.targetedEntity == TargetType.SameAsTarget) {
                    if (World.instance.ContainsState(st.stateType, target) == false) cost++;
                }
            }

            cost += actionData.baseCost;
            return cost;
        }
    }
}

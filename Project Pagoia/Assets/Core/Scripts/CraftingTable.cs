﻿using UnityEngine;

namespace Core.Scripts
{
    public class CraftingTable : MonoBehaviour
    {
        public GameObject itemPrefab { get; set; }

        public Transform craftSpawn;
    }
}

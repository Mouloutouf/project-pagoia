using UnityEngine;
using UnityEngine.AI;

namespace Features.Scripts
{
    public class AgentMovement : MonoBehaviour, IMoveBehavior
    {
        public NavMeshAgent navMeshAgent;

        public void MoveTo(Vector3 _dest)
        {
            navMeshAgent.SetDestination(_dest);
        }
    }
}

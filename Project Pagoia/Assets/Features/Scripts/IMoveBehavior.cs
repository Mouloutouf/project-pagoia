﻿using UnityEngine;

namespace Features.Scripts
{
    public interface IMoveBehavior
    {
        void MoveTo(Vector3 _dest);
    }
}
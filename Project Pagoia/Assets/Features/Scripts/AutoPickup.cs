using Core.Scripts;
using UnityEngine;

namespace Features.Scripts
{
    public class AutoPickup : MonoBehaviour
    {
        public Agent agent;

        public Inventory inventory;

        public float pickupRadius;
        public LayerMask autoPickupMask;

        private void Update()
        {
            var elements = Physics.OverlapSphere(this.transform.position, pickupRadius, autoPickupMask);

            for (var i = 0; i < elements.Length; i++)
            {
                var element = elements[i].GetComponentInParent<Entity>();

                if (element.EntityProperties.Has(EntityProperty.OreCrystal))
                {
                    inventory.crystals.Add(element);
                    element.model.SetActive(false);
                    World.instance.AddState(StateType.Accessible, element, agent.entity);
                }
                else if (element.EntityProperties.Has(EntityProperty.Pickaxe))
                {
                    inventory.pickaxe = element;
                    element.model.SetActive(false);
                    World.instance.AddState(StateType.Accessible, element, agent.entity);
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(this.transform.position, pickupRadius);
        }
    }
}
